<refentry id="FoObject">
<refmeta>
<refentrytitle role="top_of_page" id="FoObject.top_of_page">FoObject</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>XMLROFF Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>FoObject</refname>
<refpurpose>libfo base object type</refpurpose>
<!--[<xref linkend="desc" endterm="desc.title"/>]-->
</refnamediv>

<refsynopsisdiv id="FoObject.synopsis" role="synopsis">
<title role="synopsis.title">Synopsis</title>

<synopsis>
                    <link linkend="FoObject-struct">FoObject</link>;
                    <link linkend="FoObjectClass">FoObjectClass</link>;
<link linkend="FoObject">FoObject</link>*           <link linkend="fo-object-new">fo_object_new</link>                       (void);
<link linkend="void">void</link>                <link linkend="fo-object-debug-dump">fo_object_debug_dump</link>                (<link linkend="gpointer">gpointer</link> object,
                                                         <link linkend="gint">gint</link> depth);
<link linkend="gchar">gchar</link>*              <link linkend="fo-object-debug-sprintf">fo_object_debug_sprintf</link>             (<link linkend="gpointer">gpointer</link> object);
<link linkend="gchar">gchar</link>*              <link linkend="fo-object-sprintf">fo_object_sprintf</link>                   (<link linkend="gpointer">gpointer</link> object);
<link linkend="void">void</link>                <link linkend="fo-object-log-error">fo_object_log_error</link>                 (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **error);
<link linkend="void">void</link>                <link linkend="fo-object-log-warning">fo_object_log_warning</link>               (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **warning);
<link linkend="void">void</link>                <link linkend="fo-object-log-debug">fo_object_log_debug</link>                 (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **debug);
<link linkend="gboolean">gboolean</link>            <link linkend="fo-object-log-or-propagate-error">fo_object_log_or_propagate_error</link>    (<link linkend="FoObject">FoObject</link> *fo_object,
                                                         <link linkend="GError">GError</link> **dest,
                                                         <link linkend="GError">GError</link> *src);
<link linkend="gboolean">gboolean</link>            <link linkend="fo-object-maybe-propagate-error">fo_object_maybe_propagate_error</link>     (<link linkend="FoObject">FoObject</link> *fo_object,
                                                         <link linkend="GError">GError</link> **dest,
                                                         <link linkend="GError">GError</link> *src,
                                                         <link linkend="gboolean">gboolean</link> continue_after_error);
<link linkend="guint">guint</link>               <link linkend="fo-object-hash">fo_object_hash</link>                      (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **error);
<link linkend="gboolean">gboolean</link>            <link linkend="fo-object-equal">fo_object_equal</link>                     (<link linkend="FoObject">FoObject</link> *a,
                                                         <link linkend="FoObject">FoObject</link> *b,
                                                         <link linkend="GError">GError</link> **error);
</synopsis>
</refsynopsisdiv>

<refsect1 id="FoObject.object-hierarchy" role="object_hierarchy">
<title role="object_hierarchy.title">Object Hierarchy</title>
<synopsis>
  <link linkend="GObject">GObject</link>
   +----FoObject
         +----<link linkend="FoNode">FoNode</link>
         +----<link linkend="FoDatatype">FoDatatype</link>
         +----<link linkend="FoContext">FoContext</link>
         +----<link linkend="FoProperty">FoProperty</link>
         +----<link linkend="FoHashTable">FoHashTable</link>
</synopsis>

</refsect1>








<refsect1 id="FoObject.description" role="desc">
<title role="desc.title">Description</title>
<para>
Top of the object hierarchy for libfo.
</para>
<para>
Extends <link linkend="GObject"><type>GObject</type></link> to add some common debugging and logging
functions.</para>
<para>

</para>
</refsect1>

<refsect1 id="FoObject.details" role="details">
<title role="details.title">Details</title>
<refsect2 id="FoObject-struct" role="struct">
<title>FoObject</title>
<indexterm zone="FoObject-struct"><primary>FoObject</primary></indexterm><programlisting>typedef struct _FoObject FoObject;</programlisting>
<para>

</para></refsect2>
<refsect2 id="FoObjectClass" role="struct">
<title>FoObjectClass</title>
<indexterm zone="FoObjectClass"><primary>FoObjectClass</primary></indexterm><programlisting>typedef struct {
  GObjectClass parent_class;

  void     (* debug_dump)	      (FoObject     *object,
				       gint          depth);
  char*    (* debug_sprintf)	      (FoObject     *object);
  char*    (* print_sprintf)	      (FoObject     *object);
  void     (* log_error)              (FoObject     *object,
				       GError      **error);
  void     (* log_warning)            (FoObject     *object,
				       GError      **warning);
  void     (* log_debug)              (FoObject     *object,
				       GError      **debug);
  gboolean (* log_or_propagate_error) (FoObject     *fo_object,
				       GError      **dest,
				       GError       *src);
  gboolean (* maybe_propagate_error)  (FoObject     *fo_object,
				       GError      **dest,
				       GError       *src,
				       gboolean      continue_after_error);
  guint    (* hash_func)              (gconstpointer key);
  gboolean (* equal_func)             (gconstpointer a,
				       gconstpointer b);
} FoObjectClass;
</programlisting>
<para>

</para></refsect2>
<refsect2 id="fo-object-new" role="function">
<title>fo_object_new ()</title>
<indexterm zone="fo-object-new"><primary>fo_object_new</primary></indexterm><programlisting><link linkend="FoObject">FoObject</link>*           fo_object_new                       (void);</programlisting>
<para>
Creates a new <link linkend="FoObject"><type>FoObject</type></link> initialized to default value.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> the new <link linkend="FoObject"><type>FoObject</type></link>.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-debug-dump" role="function">
<title>fo_object_debug_dump ()</title>
<indexterm zone="fo-object-debug-dump"><primary>fo_object_debug_dump</primary></indexterm><programlisting><link linkend="void">void</link>                fo_object_debug_dump                (<link linkend="gpointer">gpointer</link> object,
                                                         <link linkend="gint">gint</link> depth);</programlisting>
<para>
Calls debug_dump method of class of <parameter>object</parameter>, if <parameter>object</parameter> is an
<link linkend="FoObject"><type>FoObject</type></link> or descendant type of <link linkend="FoObject"><type>FoObject</type></link>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>object</parameter>&nbsp;:</term>
<listitem><simpara> The <link linkend="FoObject"><type>FoObject</type></link> object.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>depth</parameter>&nbsp;:</term>
<listitem><simpara>  Indent level to add to the output.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-debug-sprintf" role="function">
<title>fo_object_debug_sprintf ()</title>
<indexterm zone="fo-object-debug-sprintf"><primary>fo_object_debug_sprintf</primary></indexterm><programlisting><link linkend="gchar">gchar</link>*              fo_object_debug_sprintf             (<link linkend="gpointer">gpointer</link> object);</programlisting>
<para>
Calls debug_sprintf method of class of <parameter>object</parameter>, if <parameter>object</parameter> is an
<link linkend="FoObject"><type>FoObject</type></link> or descendant type of <link linkend="FoObject"><type>FoObject</type></link>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>object</parameter>&nbsp;:</term>
<listitem><simpara> The <link linkend="FoObject"><type>FoObject</type></link> object.
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> Result of debug_sprintf method of class of <parameter>object</parameter>.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-sprintf" role="function">
<title>fo_object_sprintf ()</title>
<indexterm zone="fo-object-sprintf"><primary>fo_object_sprintf</primary></indexterm><programlisting><link linkend="gchar">gchar</link>*              fo_object_sprintf                   (<link linkend="gpointer">gpointer</link> object);</programlisting>
<para>
Calls sprintf method of class of <parameter>object</parameter>, if <parameter>object</parameter> is an
<link linkend="FoObject"><type>FoObject</type></link> or descendant type of <link linkend="FoObject"><type>FoObject</type></link>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>object</parameter>&nbsp;:</term>
<listitem><simpara> The <link linkend="FoObject"><type>FoObject</type></link> object.
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> Result of sprintf method of class of <parameter>object</parameter>.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-log-error" role="function">
<title>fo_object_log_error ()</title>
<indexterm zone="fo-object-log-error"><primary>fo_object_log_error</primary></indexterm><programlisting><link linkend="void">void</link>                fo_object_log_error                 (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **error);</programlisting>
<para>
Calls the 'log_error' method of the class of <parameter>object</parameter>.
</para>
<para>
The called method clears <parameter>error</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>object</parameter>&nbsp;:</term>
<listitem><simpara> <link linkend="FoObject"><type>FoObject</type></link> that is subject of <parameter>error</parameter>.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>error</parameter>&nbsp;:</term>
<listitem><simpara>  <link linkend="GError"><type>GError</type></link> with information about error that occurred.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-log-warning" role="function">
<title>fo_object_log_warning ()</title>
<indexterm zone="fo-object-log-warning"><primary>fo_object_log_warning</primary></indexterm><programlisting><link linkend="void">void</link>                fo_object_log_warning               (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **warning);</programlisting>
<para>
Calls the 'log_warning' method of the class of <parameter>object</parameter>.
</para>
<para>
The called method clears <parameter>error</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>object</parameter>&nbsp;:</term>
<listitem><simpara> <link linkend="FoObject"><type>FoObject</type></link> that is subject of <parameter>error</parameter>.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>warning</parameter>&nbsp;:</term>
<listitem><simpara> <link linkend="GError"><type>GError</type></link> with information about warning that occurred.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-log-debug" role="function">
<title>fo_object_log_debug ()</title>
<indexterm zone="fo-object-log-debug"><primary>fo_object_log_debug</primary></indexterm><programlisting><link linkend="void">void</link>                fo_object_log_debug                 (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **debug);</programlisting>
<para>
Calls the 'log_debug' method of the class of <parameter>object</parameter>.
</para>
<para>
The called method clears <parameter>error</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>object</parameter>&nbsp;:</term>
<listitem><simpara> <link linkend="FoObject"><type>FoObject</type></link> that is subject of <parameter>error</parameter>.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>debug</parameter>&nbsp;:</term>
<listitem><simpara>  <link linkend="GError"><type>GError</type></link> with debugging information.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-log-or-propagate-error" role="function">
<title>fo_object_log_or_propagate_error ()</title>
<indexterm zone="fo-object-log-or-propagate-error"><primary>fo_object_log_or_propagate_error</primary></indexterm><programlisting><link linkend="gboolean">gboolean</link>            fo_object_log_or_propagate_error    (<link linkend="FoObject">FoObject</link> *fo_object,
                                                         <link linkend="GError">GError</link> **dest,
                                                         <link linkend="GError">GError</link> *src);</programlisting>
<para>
If can propagate <parameter>src</parameter> to <parameter>dest</parameter>, do so, otherwise log <parameter>src</parameter> using
<link linkend="fo-object-log-error"><function>fo_object_log_error()</function></link>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>fo_object</parameter>&nbsp;:</term>
<listitem><simpara> <link linkend="FoObject"><type>FoObject</type></link> that is the subject of <parameter>src</parameter>.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>dest</parameter>&nbsp;:</term>
<listitem><simpara>      <link linkend="GError"><type>GError</type></link> to which to propagate <parameter>src</parameter>, or NULL.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>src</parameter>&nbsp;:</term>
<listitem><simpara>       <link linkend="GError"><type>GError</type></link> with information about error that occurred.
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> <link linkend="TRUE:CAPS"><literal>TRUE</literal></link> if error propagated, otherwise <link linkend="FALSE:CAPS"><literal>FALSE</literal></link>.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-maybe-propagate-error" role="function">
<title>fo_object_maybe_propagate_error ()</title>
<indexterm zone="fo-object-maybe-propagate-error"><primary>fo_object_maybe_propagate_error</primary></indexterm><programlisting><link linkend="gboolean">gboolean</link>            fo_object_maybe_propagate_error     (<link linkend="FoObject">FoObject</link> *fo_object,
                                                         <link linkend="GError">GError</link> **dest,
                                                         <link linkend="GError">GError</link> *src,
                                                         <link linkend="gboolean">gboolean</link> continue_after_error);</programlisting>
<para>
If <parameter>continue_after_error</parameter> is <link linkend="FALSE:CAPS"><literal>FALSE</literal></link> and can propagate <parameter>src</parameter> to <parameter>dest</parameter>,
then do so, otherwise log <parameter>src</parameter> using <link linkend="fo-object-log-error"><function>fo_object_log_error()</function></link>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>fo_object</parameter>&nbsp;:</term>
<listitem><simpara>            <link linkend="FoObject"><type>FoObject</type></link> that is the subject of <parameter>src</parameter>.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>dest</parameter>&nbsp;:</term>
<listitem><simpara>                 <link linkend="GError"><type>GError</type></link> to which to propagate <parameter>src</parameter>, or NULL.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>src</parameter>&nbsp;:</term>
<listitem><simpara>                  <link linkend="GError"><type>GError</type></link> with information about error that occurred.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>continue_after_error</parameter>&nbsp;:</term>
<listitem><simpara> Whether or not to continue after an error.
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> <link linkend="TRUE:CAPS"><literal>TRUE</literal></link> if error propagated, otherwise <link linkend="FALSE:CAPS"><literal>FALSE</literal></link>.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-hash" role="function">
<title>fo_object_hash ()</title>
<indexterm zone="fo-object-hash"><primary>fo_object_hash</primary></indexterm><programlisting><link linkend="guint">guint</link>               fo_object_hash                      (<link linkend="FoObject">FoObject</link> *object,
                                                         <link linkend="GError">GError</link> **error);</programlisting>
<para>
Create a hash code for the object.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>object</parameter>&nbsp;:</term>
<listitem><simpara> <link linkend="FoObject"><type>FoObject</type></link> for which to get hash value.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>error</parameter>&nbsp;:</term>
<listitem><simpara>  <link linkend="GError"><type>GError</type></link> with information about error that occurred.
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> Hash code for the object.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="fo-object-equal" role="function">
<title>fo_object_equal ()</title>
<indexterm zone="fo-object-equal"><primary>fo_object_equal</primary></indexterm><programlisting><link linkend="gboolean">gboolean</link>            fo_object_equal                     (<link linkend="FoObject">FoObject</link> *a,
                                                         <link linkend="FoObject">FoObject</link> *b,
                                                         <link linkend="GError">GError</link> **error);</programlisting>
<para>
Compare <parameter>a</parameter> to <parameter>b</parameter> using the 'equal_func' of the class of <parameter>a</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>a</parameter>&nbsp;:</term>
<listitem><simpara>     First object to compare.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>b</parameter>&nbsp;:</term>
<listitem><simpara>     Second object to compare.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>error</parameter>&nbsp;:</term>
<listitem><simpara> <link linkend="GError"><type>GError</type></link> with information about error that occurred.
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> <link linkend="TRUE:CAPS"><literal>TRUE</literal></link> if the objects are equal.
</simpara></listitem></varlistentry>
</variablelist></refsect2>

</refsect1>




</refentry>
