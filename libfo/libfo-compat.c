/* Fo
 * libfo-compat.c: LibFo compatibility processing
 *
 * Copyright (C) 2006 Sun Microsystems
 * Copyright (C) 2007 Menteith Consulting
 *
 * See COPYING for the status of this software.
 */

#include "libfo/fo-xml-doc-private.h"
#include "libfo/libfo-compat.h"
#include "libfo/fo-xslt-transformer.h"
#include <libxml/xmlversion.h>
#include <libxml/xmlmemory.h>
#include <libxml/debugXML.h>
#include <libxml/HTMLtree.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>

const char *libfo_compat_error_messages [] = {
  N_("libfo-compat error")
};

static const gchar* stylesheet =
"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
"<!-- libfo -->\n"
"<!-- XSLT stylesheet to bring an XSL FO document into compatibility\n"
"     with the capabilities of the libfo library of the xmlroff XSL\n"
"     Formatter. -->\n"
"\n"
"<!-- Author: Tony Graham -->\n"
"\n"
"<!--\n"
"     Copyright (c) 2003-2006 Sun Microsystems. All Rights Reserved.\n"
"     Copyright (c) 2007 Menteith Consulting\n"
"\n"
"     Redistribution and use in source and binary forms, with or without\n"
"     modification, are permitted provided that the following conditions are\n"
"     met:\n"
"\n"
"      - Redistributions of source code must retain the above copyright\n"
"        notice, this list of conditions and the following disclaimer.\n"
"\n"
"      - Redistribution in binary form must reproduce the above copyright\n"
"        notice, this list of conditions and the following disclaimer in the\n"
"        documentation and/or other materials provided with the\n"
"        distribution.\n"
"\n"
"     Neither the name of Sun Microsystems or the names of contributors may\n"
"     be used to endorse or promote products derived from this software\n"
"     without specific prior written permission.\n"
"\n"
"     This software is provided \"AS IS,\" without a warranty of any kind. ALL\n"
"     EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,\n"
"     INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A\n"
"     PARTICULAR PURPOSE OR NON INFRINGEMENT, ARE HEREBY EXCLUDED. SUN\n"
"     MICROSYSTEMS AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES\n"
"     SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING\n"
"     THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN MICROSYSTEMS OR\n"
"     ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR\n"
"     DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE\n"
"     DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY,\n"
"     ARISING OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN\n"
"     MICROSYSTEMS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.\n"
"\n"
"     You acknowledge that this Software is not designed, licensed or\n"
"     intended for use in the design, construction, operation or maintenance\n"
"     of any nuclear facility.\n"
"-->\n"
"<xsl:stylesheet\n"
"  xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
"  xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
"  version=\"1.0\">\n"
"\n"
"  <xsl:output method=\"xml\"/>\n"
"\n"
"  <xsl:param name=\"verbose\" select=\"false()\"/>\n"
"  <xsl:param name=\"keep-threshold\" select=\"20\"/>\n"
"  \n"
"  <xsl:template match=\"text()[normalize-space() = '']\">\n"
"    <xsl:variable name=\"previous-is-inline\">\n"
"      <xsl:choose>\n"
"        <xsl:when test=\"preceding-sibling::*[1]\">\n"
"          <xsl:call-template name=\"is-inline\">\n"
"            <xsl:with-param name=\"fo\" select=\"preceding-sibling::*[1]\"/>\n"
"          </xsl:call-template>\n"
"        </xsl:when>\n"
"        <xsl:otherwise>no</xsl:otherwise>\n"
"      </xsl:choose>\n"
"    </xsl:variable>\n"
"\n"
"    <xsl:variable name=\"following-is-inline\">\n"
"      <xsl:choose>\n"
"        <xsl:when test=\"following-sibling::*[1]\">\n"
"          <xsl:call-template name=\"is-inline\">\n"
"            <xsl:with-param name=\"fo\" select=\"following-sibling::*[1]\"/>\n"
"          </xsl:call-template>\n"
"        </xsl:when>\n"
"        <xsl:otherwise>no</xsl:otherwise>\n"
"      </xsl:choose>\n"
"    </xsl:variable>\n"
"\n"
"    <xsl:if test=\"$previous-is-inline = 'yes' or $following-is-inline = 'yes'\">\n"
"      <xsl:value-of select=\".\"/>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"  \n"
"  <xsl:template name=\"is-inline\">\n"
"    <xsl:param name=\"fo\"/>\n"
"    \n"
"    <xsl:variable name=\"local-name\" select=\"local-name($fo)\"/>\n"
"    <xsl:choose>\n"
"      <xsl:when\n"
"        test=\"$local-name = 'bidi-override' or\n"
"        $local-name = 'character' or\n"
"        $local-name = 'external-graphic' or\n"
"        $local-name = 'instream-foreign-object' or\n"
"        $local-name = 'inline' or\n"
"        $local-name = 'inline-container' or\n"
"        $local-name = 'leader' or\n"
"        $local-name = 'page-number' or\n"
"        $local-name = 'page-number-citation' or\n"
"        $local-name = 'page-number-citation-last' or\n"
"        $local-name = 'scaling-value-citation' or\n"
"        $local-name = 'basic-link' or\n"
"        $local-name = 'multi-toggle' or\n"
"        $local-name = 'index-page-citation-list' or\n"
"        $local-name = 'multi-switch' or\n"
"        $local-name = 'multi-properties' or\n"
"        $local-name = 'index-range-begin' or\n"
"        $local-name = 'index-range-end' or\n"
"        $local-name = 'wrapper' or\n"
"        $local-name = 'retrieve-marker' or\n"
"        $local-name = 'float' or\n"
"        $local-name = 'footnote'\">yes</xsl:when>\n"
"      <xsl:otherwise>no</xsl:otherwise>\n"
"    </xsl:choose>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:list-item-label/text()\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Correcting text child of fo:list-item-label by removing text</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"text()[. = '&#xA0;']\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing \"&amp;#xA0;\".</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"@display-align[.='middle']\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Correcting 'display-align=\"middle\"' to 'display-align=\"center\"'.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:attribute name=\"{name()}\">center</xsl:attribute>\n"
"  </xsl:template>\n"
"\n"
"\n"
"  <!-- Unsupported properties to be removed. -->\n"
"  <xsl:template match=\"@background-attachment |\n"
"           @background-image |\n"
"           @background-position-horizontal |\n"
"           @background-position-vertical |\n"
"           @background-repeat |\n"
"           @blank-or-not-blank |\n"
"           @column-count |\n"
"           @column-gap |\n"
"           @content-type |\n"
"           @extent |\n"
"           @external-destination |\n"
"           @font-selection-strategy |\n"
"           @force-page-count |\n"
"           @height |\n"
"           @hyphenate |\n"
"           @hyphenation-character |\n"
"           @hyphenation-push-character-count |\n"
"           @hyphenation-remain-character-count |\n"
"           @initial-page-number |\n"
"           @internal-destination |\n"
"           @language |\n"
"           @last-line-end-indent |\n"
"           @leader-alignment |\n"
"           @leader-pattern |\n"
"           @leader-pattern-width |\n"
"           @line-height-shift-adjustment |\n"
"           @margin |\n"
"           @odd-or-even |\n"
"           @page-position |\n"
"           @text-align-last\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing unsupported '<xsl:value-of select=\"name()\"/>' property.</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"@keep-together |\n"
"    @keep-together.within-line |\n"
"    @keep-together.within-column |\n"
"    @keep-together.within-page |\n"
"    @keep-with-previous |\n"
"    @keep-with-previous.within-line |\n"
"    @keep-with-previous.within-column |\n"
"    @keep-with-previous.within-page |\n"
"    @keep-with-next |\n"
"    @keep-with-next.within-line |\n"
"    @keep-with-next.within-column |\n"
"    @keep-with-next.within-page\">\n"
"    <xsl:choose>\n"
"      <xsl:when test=\"node() &lt; $keep-threshold\">\n"
"        <xsl:attribute name=\"{name()}\">auto</xsl:attribute>\n"
"      </xsl:when>\n"
"      <xsl:otherwise>\n"
"        <xsl:attribute name=\"{name()}\">\n"
"          <xsl:value-of select=\".\"/>\n"
"        </xsl:attribute>\n"
"      </xsl:otherwise>\n"
"    </xsl:choose>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"@*\">\n"
"    <xsl:attribute name=\"{name()}\">\n"
"      <xsl:value-of select=\".\"/>\n"
"    </xsl:attribute>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:block-container[@reference-orientation]\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing 'fo:block-container' with unsupported 'reference-orientation' property.</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:block-container/text()\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Correcting 'fo:block-container' containing only text.</xsl:message>\n"
"    </xsl:if>\n"
"    <fo:block>\n"
"      <xsl:copy/>\n"
"    </fo:block>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:region-body/@region-name\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing 'fo:region-body' region-name attribute.</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:float\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing unsupported 'fo:float'.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:apply-templates/>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:footnote\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing unsupported 'fo:footnote'.</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:inline-container/fo:block\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing unsupported 'fo:block' in 'fo:inline-container'.</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:simple-page-master[name(child::*[1]) = 'fo:region-before' and\n"
"    name(child::*[2]) = 'fo:region-after' and\n"
"    name(child::*[3]) = 'fo:region-body']\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Correcting 'fo:region-before', 'fo:region-after', and 'fo:region-body' order.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:copy>\n"
"      <xsl:apply-templates select=\"@*\"/>\n"
"      <xsl:apply-templates select=\"fo:region-body\"/>\n"
"      <xsl:apply-templates select=\"fo:region-before\"/>\n"
"      <xsl:apply-templates select=\"fo:region-after\"/>\n"
"    </xsl:copy>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:root[name(child::*[1]) = 'fo:declarations' and\n"
"    name(child::*[2]) = 'fo:layout-master-set']\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Correcting 'fo:declarations' and 'fo:layout-master-set' order.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:copy>\n"
"      <xsl:apply-templates select=\"@*\"/>\n"
"      <xsl:apply-templates select=\"fo:layout-master-set\"/>\n"
"      <xsl:apply-templates select=\"fo:declarations\"/>\n"
"      <xsl:apply-templates select=\"fo:page-sequence\"/>\n"
"    </xsl:copy>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:static-content\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing unsupported 'fo:static-content'.</xsl:message>\n"
"    </xsl:if>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:table[@table-layout='fixed' and not(@width)]\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Auto table layout unsupported. Adding 'width' property.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:copy>\n"
"      <xsl:attribute name=\"width\">100%</xsl:attribute>\n"
"      <xsl:apply-templates select=\"@*\"/>\n"
"      <xsl:apply-templates select=\"node()\"/>\n"
"    </xsl:copy>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:table[not(@table-layout) and @width]\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Auto table layout unsupported. Adding 'table-layout' property.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:copy>\n"
"      <xsl:attribute name=\"table-layout\">fixed</xsl:attribute>\n"
"      <xsl:apply-templates select=\"@*\"/>\n"
"      <xsl:apply-templates select=\"node()\"/>\n"
"    </xsl:copy>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:table[not(@table-layout) and not(@width)]\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Auto table layout unsupported. Adding 'table-layout' and 'width' properties.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:copy>\n"
"      <xsl:attribute name=\"table-layout\">fixed</xsl:attribute>\n"
"      <xsl:attribute name=\"width\">\n"
"        <xsl:choose>\n"
"          <xsl:when test=\"count(fo:table-column/@column-width) != 0\">\n"
"            <xsl:if test=\"$verbose\">\n"
"              <xsl:message>Computing table-width from 'fo:table-column/@column-width' properties.</xsl:message>\n"
"            </xsl:if>\n"
"            <xsl:for-each select=\"fo:table-column/@column-width\">\n"
"              <xsl:value-of select=\".\"/>\n"
"              <xsl:if test=\"position() != last()\">\n"
"                <xsl:text> + </xsl:text>\n"
"              </xsl:if>\n"
"            </xsl:for-each>\n"
"          </xsl:when>\n"
"          <xsl:otherwise>\n"
"            <xsl:if test=\"$verbose\">\n"
"              <xsl:message>Using 'width=\"100%\"'.</xsl:message>\n"
"            </xsl:if>\n"
"            <xsl:text>100%</xsl:text>\n"
"          </xsl:otherwise>\n"
"        </xsl:choose>\n"
"      </xsl:attribute>\n"
"      <xsl:apply-templates select=\"@*\"/>\n"
"      <xsl:apply-templates select=\"node()\"/>\n"
"    </xsl:copy>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"fo:wrapper[fo:list-item]\">\n"
"    <xsl:if test=\"$verbose\">\n"
"      <xsl:message>Removing 'fo:wrapper' used in an unsupported context.</xsl:message>\n"
"    </xsl:if>\n"
"    <xsl:apply-templates/>\n"
"  </xsl:template>\n"
"\n"
"  <xsl:template match=\"*\">\n"
"    <xsl:copy>\n"
"      <xsl:apply-templates select=\"@*\"/>\n"
"      <xsl:apply-templates select=\"node()\"/>\n"
"    </xsl:copy>\n"
"  </xsl:template>\n"
"\n"
"</xsl:stylesheet>\n";

/**
 * libfo_compat_get_stylesheet:
 *
 * Get the built-in compatibility stylesheet as a single, rather long
 * string.
 * 
 * Return value: The built-in stylesheet.
 **/
const gchar*
libfo_compat_get_stylesheet (void)
{
  return stylesheet;
}

/**
 * libfo_compat_make_compatible:
 * @result_tree:   Result of previous parse or transformation.
 * @libfo_context: #FoLibfoContext.
 * @error:         Indication of any error that occurred.
 * 
 * Make @result_tree compatible with libfo by applying the built-in
 * copy of the 'libfo-compat.xsl' stylesheet.
 *
 * Return value: A new result tree.
 **/
FoXmlDoc *
libfo_compat_make_compatible (FoXmlDoc       *result_tree,
			      FoLibfoContext *libfo_context,
			      GError        **error)
{
  FoXmlDoc *compatible_result;
  GError *tmp_error = NULL;
  gchar* base = NULL;

  g_return_val_if_fail (result_tree != NULL, NULL);

  base = fo_xml_doc_get_base (result_tree);

  FoXmlDoc *stylesheet_doc = fo_xml_doc_new_from_string (stylesheet,
							 base,
							 NULL,
							 libfo_context,
							 &tmp_error);

  if (tmp_error != NULL)
    {
      g_propagate_error (error,
			 tmp_error);
      return NULL;
    }

  compatible_result = fo_xslt_transformer_do_transform (result_tree,
							stylesheet_doc,
							&tmp_error);
  
  fo_xml_doc_set_base (compatible_result,
		       base);
  g_free (base);

  if (tmp_error != NULL)
    {
      g_propagate_error (error,
			 tmp_error);
      return NULL;
    }

  return compatible_result;
}
